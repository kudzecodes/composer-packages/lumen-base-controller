<?php

namespace Kudze\LumenBaseController\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response as ResponseTypes;

class Controller extends BaseController
{
    protected function noContentResponse(): Response
    {
        return response('', ResponseTypes::HTTP_NO_CONTENT);
    }

    protected function unauthorizedResponse(array $payload, int $code = ResponseTypes::HTTP_UNAUTHORIZED): JsonResponse
    {
        return new JsonResponse($payload, $code);
    }
}