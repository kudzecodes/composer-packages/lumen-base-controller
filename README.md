# Lumen Base Controller

## Description

Mainly, just a standard base controller for my projects.
This package is used by my other packages.

## Installation

1. Install this package as composer package into lumen project.